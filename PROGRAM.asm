.model tiny

.code
org 100h


start:
	mov si, 0h
	mov bx, 0h
	mov ah, 09h
	lea dx, pesan_input
	int 21h

input:
    mov ah, 01h
    int 21h
    cmp al, 13
    je proses
	
	cmp al, 8
	je hapus
    
    mov kalimat[si], al
    inc si
    jmp input
    

hapus:

	cmp si, 0
	je input
	dec si
	mov kalimat[si], 0
	
	mov ah, 2h
	mov dl, 32
	int 21h
	
	mov dl, 8
	int 21h
	
	jmp input
	
	
proses:

	cmp si, 0
	je output
	
    mov cx, si
    mov di, 0h
    mov al, 0h  ; al = boolean masuk
    
    perulangan:
        mov dl, kalimat[di] ;   dl = huruf
        inc di
        
        cmp dl, " " ;   membandingkan huruf dengan karakter spasi
        jne p1  ;   lompat ke p1 jika huruf tidak sama dengan spasi
        je p2   ;   lompat ke p2 jika huruf sama dengan spasi
        
        p1:
            cmp al,0h
            je p1_1
            jne p1_2
            
            
        p1_1:   ;   jika huruf tidak sama dengan spasi dan !masuk
            mov al, 1h        
            inc bx
            jmp continue
            
        
        p1_2:
            jmp continue
            
            
        p2: ;   jika huruf sama dengan spasi   
            mov al, 0h
            jmp continue
            
        
        
    continue:
    loop perulangan
    

output:

    
    mov ah, 09h
    lea dx, pesan_output
    int 21h
    
    mov ax, bx
    call print

ret
print PROC     
      
    ;initilize count 
    mov cx,0h
    mov dx,0h 
    label1: 
        ; if ax is zero 
        cmp ax,0 
        je print1       
          
        ;initilize bx to 10 
        mov bx,10         
          
        ; extract the last digit 
        div bx                   
          
        ;push it in the stack 
        push dx               
          
        ;increment the count 
        inc cx               
          
        ;set dx to 0  
        xor dx,dx 
        jmp label1 
    print1: 
        ;check if count  
        ;is greater than zero 
        cmp cx,0 
        je exit
          
        ;pop the top of stack 
        pop dx 
          
        ;add 48 so that it  
        ;represents the ASCII 
        ;value of digits 
        add dx,48 
          
        ;interuppt to print a 
        ;character 
        mov ah,02h 
        int 21h 
          
        ;decrease the count 
        dec cx 
        jmp print1 
exit: 
ret 
print ENDP	

.data
	masuk db 0h
	pesan_input db "Masukkan kalimat : ", 13, 10, "$"
	pesan_output db 13, 10, "Jumlah kata dalam kalimat : $"
	kalimat db ?
end start
